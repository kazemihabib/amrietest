package com.example.amrietest

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.util.Log
import com.google.android.exoplayer2.util.Util

const val BUY_PRODUCT = "buy_product"
class PlayerActivity : AppCompatActivity(), Player.EventListener {

    lateinit var player: SimpleExoPlayer

    lateinit var productKey: String
    var isTrial: Boolean = false
    lateinit var videoUrl: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)

        videoUrl = intent.getStringExtra(VIDEO_URL)
        isTrial = intent.getBooleanExtra(IS_TRIAL, true)
        productKey = intent.getStringExtra(PRODUCT_KEY)

        player = ExoPlayerFactory.newSimpleInstance(this)

        val playerView: PlayerView = findViewById(R.id.video_view)

        playerView.player = player
        player.playWhenReady = true
        player.addListener(this)

        playDownloaded(videoUrl)
    }

    private fun playDownloaded(uriString: String) {
        val downloadCache = (application as App).getDownloadCache()
        val upstreamDataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, "AmrieTest"))
        val dataSourceFactory = CacheDataSourceFactory( downloadCache, upstreamDataSourceFactory)
        val mediaSource = ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(uriString))
        player.prepare(mediaSource)
    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        super.onPlayerStateChanged(playWhenReady, playbackState)
        when(playbackState) {
            Player.STATE_ENDED -> {
                videoEnded()
            }
        }
    }

    private fun videoEnded() {
        if (isTrial) {

            val builder = AlertDialog.Builder(this)
            builder.apply {
                setMessage(R.string.dialog_video_trial_end_message)
                setCancelable(false)
                setPositiveButton(R.string.dialog_video_buy) { dialog, which ->
                    run {
                        val returnIntent = Intent()
                        returnIntent.putExtra(PRODUCT_KEY, productKey)
                        returnIntent.putExtra(VIDEO_URL, videoUrl)
                        setResult(Activity.RESULT_OK, returnIntent)
                        finish()
                    }
                }

                setNegativeButton(R.string.dialog_cancel) { dialog, which ->
                    run {
                        val returnIntent = Intent()
                        setResult(Activity.RESULT_CANCELED, returnIntent)
                        finish()
                    }
                }
            }

            builder.show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::player.isInitialized) player.release()
    }
}
