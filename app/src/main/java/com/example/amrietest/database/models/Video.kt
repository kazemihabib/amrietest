package com.example.amrietest.database.models

import android.net.Uri
import androidx.room.Entity

@Entity(primaryKeys = ["productKey"])
data class Video(
    val productKey: String,
    val name: String,
    val price: String,
    val imageUrl: String,
    val description: String,
    val purchase: Boolean,
    val trialUrl: String,
    val videoUrl: String
)

