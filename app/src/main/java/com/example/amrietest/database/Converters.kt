package com.example.amrietest.database

import android.net.Uri
import androidx.room.TypeConverter

class Converters {
    @TypeConverter
    fun uriToString(uri: Uri?): String = uri.toString()
    @TypeConverter fun stringToUri(value: String?): Uri? {
        if (value == null)
            return null
        return Uri.parse(value)
    }
}
