package com.example.amrietest.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.amrietest.database.models.Video

@Dao
interface VideoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(video: Video)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(videos: List<Video>)

    @Query("SELECT * FROM Video")
    fun load(): LiveData<List<Video>>

    @Query("SELECT * FROM Video WHERE purchase = 1")
    fun loadPurchased(): List<Video>

    @Query("Update Video SET purchase = 1 where productKey=:productId")
    fun addPurchased(productId: String)
}