package com.example.amrietest.database

import android.content.Context
import androidx.room.*
import com.example.amrietest.database.models.Video

@Database(entities = [ Video::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class VideoDb: RoomDatabase() {
    abstract fun videoDao(): VideoDao

    companion object {

        @Volatile private var INSTANCE: VideoDb? = null
        fun getDatabase(context: Context): VideoDb =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        fun buildDatabase(context: Context) = Room.databaseBuilder(context.applicationContext, VideoDb::class.java, "VideoDb").build()
    }
}


