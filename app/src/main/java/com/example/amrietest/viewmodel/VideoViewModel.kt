package com.example.amrietest.viewmodel

import android.app.Application
import android.os.Parcel
import android.os.Parcelable
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.amrietest.App
import com.example.amrietest.DownloadService
import com.example.amrietest.database.models.Video
import com.example.amrietest.repository.VideoRepository
import com.google.android.exoplayer2.offline.Download
import com.google.android.exoplayer2.offline.DownloadManager
import com.google.android.exoplayer2.util.Log



class VideoViewModel(application: Application): AndroidViewModel(application) {
    private val videoRepository: VideoRepository = VideoRepository(application)
    val isApiLoading = ObservableBoolean(false)

    fun getVideoList(purchasedList: List<String>): LiveData<List<Video>> = videoRepository.getVideoList(isApiLoading, purchasedList)

    fun refreshVideoList(purchasedList: List<String>) = videoRepository.refreshVideoList(isApiLoading, purchasedList)
    fun refreshVideoListEmptyPurchase() = videoRepository.refreshVideoList(isApiLoading, listOf())

    fun addPurchasedItem(product_id: String) = videoRepository.addPurchasedItem(product_id)

    fun downloadManagerListener() {
        val downloadManager = getApplication<App>().getDownloadManager()
        downloadManager.addListener(
            object : DownloadManager.Listener {

                // Override methods of interest here.
                override fun onDownloadChanged(downloadManager: DownloadManager, download: Download) {
                    Log.d("AmrieTest", "listener: " + download.request.uri + download)
//                    for (listener in listeners) {
//                        listener.onDownloadsChanged()
//                    }
                }
            })
    }
}

