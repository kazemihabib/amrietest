package com.example.amrietest

import android.app.Application
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.offline.DownloadManager
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.Cache
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import java.io.File


private const val TAG = "AmrieTest"
private const val DOWNLOAD_ACTION_FILE = "actions"
private const val DOWNLOAD_TRACKER_ACTION_FILE = "tracked_actions"
private const val DOWNLOAD_CONTENT_DIRECTORY = "downloads"
private const val USERAGENT = "AmrieTest"
const val BAZAR_BASE_64: String = "MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwDC3pvNuRP/GR3mOhnDlvzl2U3rtwxuEqpEY/3jtvIHvcMMOqI5am20OkSsz7aLSucmRa34lDEhjsoI7zM8pR/dn4fYWVILA4G4eaSad/zNNzFTNq/dzluvo5PDtpoXsRBS8ZaYkZMtj2npRUNR5RZjCfFJffb9kbLQcYEY2XPMYq6qkjhM7DZlS3j0zPBoh2CVZLAaDyldgPk40Rovvr+Z1cDegV87RyNqfJGuFy0CAwEAAQ=="

class App: Application() {
    private lateinit var databaseProvider: ExoDatabaseProvider
    private lateinit var downloadCache: Cache
    private var downloadDirectory: File? = null


    fun getDatabaseProvider(): ExoDatabaseProvider {
        if (!::databaseProvider.isInitialized) databaseProvider = ExoDatabaseProvider(this)
        return databaseProvider
    }

    @Synchronized
    fun getDownloadCache(): Cache {
        if (!::downloadCache.isInitialized) {
            val downloadContentDirectory = File(getDownloadDirectory(), DOWNLOAD_CONTENT_DIRECTORY)
            downloadCache = SimpleCache(downloadContentDirectory, NoOpCacheEvictor(), getDatabaseProvider())
        }
        return downloadCache
    }

    private fun getDownloadDirectory(): File? {
        if (downloadDirectory == null) {
            downloadDirectory = getExternalFilesDir(null)

            if (downloadDirectory == null) {
                downloadDirectory = filesDir
            }
        }
        return downloadDirectory
    }

    fun getDownloadManager(): DownloadManager {
        val databaseProvider = getDatabaseProvider()
        val downloadCache = getDownloadCache()
        val dataSourceFactory = DefaultHttpDataSourceFactory("AmrieTest")
        val downloadManager = DownloadManager(this, databaseProvider, downloadCache, dataSourceFactory)
        downloadManager.maxParallelDownloads = 30
        return downloadManager
    }

}