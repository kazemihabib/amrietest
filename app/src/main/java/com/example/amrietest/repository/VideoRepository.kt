package com.example.amrietest.repository

import android.content.Context
import android.provider.Settings
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import com.example.amrietest.api.VideoList
import com.example.amrietest.api.VideoService
import com.example.amrietest.database.VideoDao
import com.example.amrietest.database.VideoDb
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VideoRepository (val context: Context) {
    private val videoDao: VideoDao


    init {
        val videoDb = VideoDb.getDatabase(context)
        videoDao = videoDb.videoDao()
    }

    fun refreshVideoList(isApiLoading: ObservableBoolean, purchasedList: List<String>) {
        isApiLoading.set(true)
        VideoService.getVideoService(context).getVideoList().enqueue(object : Callback<VideoList> {
            override fun onFailure(call: Call<VideoList>, t: Throwable) {
                Log.d("AmrieTest", "VideoRepository failed")
            }

            override fun onResponse(call: Call<VideoList>, response: Response<VideoList>) {
                GlobalScope.launch {
                    val purchasedVideos = videoDao.loadPurchased()
                    response.body()?.videos?.forEach { apiVideo ->
                        val purchased = if (purchasedList.find { it == apiVideo.productKey}!=null) true
                        else purchasedVideos.find {it.productKey == apiVideo.productKey}!=null

                        videoDao.insert(com.example.amrietest.database.models.Video(apiVideo.productKey, apiVideo.name, apiVideo.price, apiVideo.image_url, apiVideo.description,  purchased, apiVideo.trial_url, apiVideo.video_url))
                    }

                    isApiLoading.set(false)
                }
            }
        })
    }

    fun getVideoList(isApiLoading: ObservableBoolean, boughtList: List<String>): LiveData<List<com.example.amrietest.database.models.Video>> {
        refreshVideoList(isApiLoading, boughtList)
        return videoDao.load()
    }

    fun addPurchasedItem(product_id: String) {
        GlobalScope.launch {
            videoDao.addPurchased(product_id)
        }
    }
}