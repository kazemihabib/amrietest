package com.example.amrietest

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.amrietest.adapter.*
import com.example.amrietest.adapter.VideoAdapter
import com.example.amrietest.database.models.Video
import com.example.amrietest.databinding.ActivityMainBinding
import com.example.amrietest.viewmodel.VideoViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.isActive
import com.example.amrietest.util.IabHelper
import com.google.android.exoplayer2.offline.DownloadRequest
import com.google.android.exoplayer2.offline.DownloadService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*


const val VIDEO_URL = "video_url"
const val IS_TRIAL = "is_trial"
const val PRODUCT_KEY = "product_key"
private const val BUY_CODE = 10001
private const val PLAY_REQUEST = 100

class MainActivity : AppCompatActivity() {
    private lateinit var adapter: VideoAdapter
    var mHelper: IabHelper? = null
    private var iabState: IABState = NotSetupped()
    lateinit var videoViewModel: VideoViewModel

    val listEventActor = GlobalScope.actor<VideoEvent> {
        for (event in channel) if (isActive) when (event) {
            is Click ->  videoItemClicked(event.item)
            is Download -> Log.d("AmrieTest", "Download " + event.item.name)
            is Shop -> Log.d("AmrieTest", "Shop " + event.item.name)
            is LongClick -> Log.d("AmrieTest", "longClicked")
        }
    }

    private fun videoItemClicked(videoItem: Video) {
        if (!videoItem.purchase) {
            val builder = AlertDialog.Builder(this@MainActivity)
            builder.apply {
                setMessage(R.string.dialog_video_message)
                setCancelable(true)
                setPositiveButton(R.string.dialog_video_watch_trial) { dialog, which ->
                    playTrial(videoItem.trialUrl, videoItem.productKey)
                }

                setNegativeButton(R.string.dialog_video_buy) { dialog, which ->
                    buyProduct(videoItem.productKey, videoItem.videoUrl)
                }
            }
            GlobalScope.launch(Dispatchers.Main) { builder.show() }
        } else {
            playVideo(videoItem.videoUrl, videoItem.productKey)
        }
    }

    private fun playVideo(videoUrlString: String, productKey: String) {
        val downloadRequest = DownloadRequest(videoUrlString, DownloadRequest.TYPE_PROGRESSIVE, Uri.parse(videoUrlString), Collections.emptyList(), null, null)
        DownloadService.sendAddDownload(this, com.example.amrietest.DownloadService::class.java, downloadRequest, false)

        val intent = Intent(this@MainActivity, PlayerActivity::class.java)
        intent.putExtra(VIDEO_URL, videoUrlString)
        intent.putExtra(IS_TRIAL, false)
        intent.putExtra(PRODUCT_KEY, productKey)
        startActivityForResult(intent, PLAY_REQUEST)
    }

    private fun playTrial(trialUrlString: String, productKey: String) {
        val downloadRequest = DownloadRequest(trialUrlString, DownloadRequest.TYPE_PROGRESSIVE, Uri.parse(trialUrlString), Collections.emptyList(), null, null)
        DownloadService.sendAddDownload(this, com.example.amrietest.DownloadService::class.java, downloadRequest, false)

        val intent = Intent(this@MainActivity, PlayerActivity::class.java)
        intent.putExtra(VIDEO_URL, trialUrlString)
        intent.putExtra(IS_TRIAL, true)
        intent.putExtra(PRODUCT_KEY, productKey)
        startActivityForResult(intent, PLAY_REQUEST)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding =  ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        videoViewModel = ViewModelProviders.of(this).get(VideoViewModel::class.java)
        binding.videoviewmodel = videoViewModel
        initIAB()

        adapter = VideoAdapter(listEventActor)
        val recycleListView = binding.videoList
        recycleListView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recycleListView.adapter = adapter
        recycleListView.layoutManager = LinearLayoutManager(this)

        videoViewModel.getVideoList(listOf()).observe(this, Observer { list -> adapter.setList(list) })
        videoViewModel.downloadManagerListener()

    }

    private fun buyProduct(productKey: String, videoUrl: String) {
        when (iabState){
            is Setupping -> {}
            is NotSetupped -> {}
            is SetupError -> {}
            is SetupedSuccessfully -> {
                mHelper?.flagEndAsync()
                mHelper?.launchPurchaseFlow(this@MainActivity, productKey, BUY_CODE) { result, info ->
                    if (result.isFailure()) {
                        Toast.makeText(this@MainActivity, R.string.purchase_error, Toast.LENGTH_SHORT).show()
                    } else {
                        videoViewModel.addPurchasedItem(info.sku)
                        playVideo(videoUrl, productKey)
                    }
                }
            }
        }
    }

    private fun checkPurchasedProducts() {
        mHelper?.queryInventoryAsync(mGotInventoryListener)
    }


    var mGotInventoryListener: IabHelper.QueryInventoryFinishedListener =
        IabHelper.QueryInventoryFinishedListener { result, inventory ->
            if (result.isFailure) {
                // handle error here
            } else {
                // does the user have the premium upgrade?
                Log.d("AmriehTest", inventory.mPurchaseMap.toString())
                val purchasedList = inventory.mPurchaseMap.map { it.key }
                videoViewModel.refreshVideoList(purchasedList)
            }
        }


    private fun initIAB() {
        // compute your public key and store it in base64EncodedPublicKey
        iabState = Setupping()
        mHelper = IabHelper(this, BAZAR_BASE_64)

        mHelper?.startSetup { result ->
            if (!result.isSuccess) {
                iabState = SetupError()
            } else {
                iabState = SetupedSuccessfully()
                checkPurchasedProducts()
            }
            mHelper?.flagEndAsync()
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        mHelper?.dispose()
        mHelper = null
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PLAY_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                buyProduct(data!!.getStringExtra(PRODUCT_KEY), data!!.getStringExtra(VIDEO_URL))
            }
        }
        // Pass on the activity result to the helper for handling
        if (!mHelper!!.handleActivityResult(requestCode, resultCode, data)) {
            Log.d("AmrieTest", "not handled")
        }
        else {
            Log.d("AmrieTest", "onActivityResult handled by IABUtil.");
        }
    }
}

sealed class IABState
class NotSetupped: IABState()
class Setupping: IABState()
class SetupError: IABState()
class SetupedSuccessfully: IABState()


