package com.example.amrietest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.amrietest.R
import com.example.amrietest.database.models.Video
import com.example.amrietest.databinding.VideoItemBinding
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.video_item.view.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.SendChannel

internal class VideoAdapter(private val eventActor: SendChannel<VideoEvent>) : RecyclerView.Adapter<VideoAdapter.ViewHolder>() {
    private var dataset: List<Video>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoAdapter.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = VideoItemBinding.inflate(inflater, parent, false )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataset?.get(position)
        holder.bind(item)
    }

    fun setList(list: List<Video>?) {
        dataset = list
        notifyDataSetChanged()
    }

    override fun getItemCount() = dataset?.size ?: 0

    @ExperimentalCoroutinesApi
    inner class ViewHolder(val binding: VideoItemBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener, View.OnLongClickListener {

        init {
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
            itemView.download.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            when(v.id) {
                R.id.list_item -> dataset?.get(layoutPosition)?.let {
                    if(!eventActor.isClosedForSend)
                        eventActor.offer(Click(it)) }

                R.id.download -> dataset?.get(layoutPosition)?.let {
                    if(!eventActor.isClosedForSend)
                        eventActor.offer(Download(it))
                }

                R.id.shop -> dataset?.get(layoutPosition)?.let {
                    if(!eventActor.isClosedForSend)
                        eventActor.offer(Shop(it))
                }
            }
        }

        override fun onLongClick(v: View): Boolean {
            dataset?.get(layoutPosition)?.let {
                if(!eventActor.isClosedForSend)
                    eventActor.offer(LongClick(it))
            }
            return true
        }


        fun bind(videoItem: Video?) {
            binding.video = videoItem
//            Picasso.get().load(videoItem?.imageUrl).resize(100, 100).into(binding.videoImage)
            Picasso.get().load(videoItem?.imageUrl).into(binding.videoImage)
            binding.executePendingBindings()
        }
    }
}

sealed class VideoEvent
class Click(val item: Video) : VideoEvent()
class LongClick(val item: Video) : VideoEvent()
class Download(val item: Video): VideoEvent()
class Shop(val item: Video): VideoEvent()
