package com.example.amrietest

import android.app.Notification
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.offline.Download
import com.google.android.exoplayer2.offline.DownloadManager
import com.google.android.exoplayer2.offline.DownloadService
import com.google.android.exoplayer2.scheduler.Scheduler
import com.google.android.exoplayer2.ui.DownloadNotificationHelper
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.SimpleCache

private const val CHANNEL_ID = "download_channel"
private const val JOB_ID = 1
private const val FOREGROUND_NOTIFICATION_ID = 1
class DownloadService : DownloadService(FOREGROUND_NOTIFICATION_ID) {

    private lateinit var downloadNotificationHelper: DownloadNotificationHelper
    override fun onCreate() {
        super.onCreate()
        downloadNotificationHelper = DownloadNotificationHelper(this, CHANNEL_ID)
    }
    override fun getDownloadManager(): DownloadManager {
        return (application as App).getDownloadManager()
    }

    override fun getForegroundNotification(downloads: MutableList<Download>?): Notification {
        return downloadNotificationHelper.buildProgressNotification(R.drawable.ic_file_download_black_24dp, null, null, downloads)
    }

    override fun getScheduler(): Scheduler? {
        return null
    }


}
