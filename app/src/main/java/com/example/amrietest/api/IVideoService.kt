package com.example.amrietest.api

import retrofit2.Call
import retrofit2.http.GET

interface IVideoService {
    @GET("/v2/5d2d755d2e00006700c57d19")
    fun getVideoList(): Call<VideoList>
}

