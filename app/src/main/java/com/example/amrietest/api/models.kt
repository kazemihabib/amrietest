package com.example.amrietest.api

data class VideoList (

    val videos : List<Video>
)

data class Video (

    val productKey : String,
    val name : String,
    val price: String,
    val description : String,
    val image_url : String,
    val trial_url : String,
    val video_url : String
)