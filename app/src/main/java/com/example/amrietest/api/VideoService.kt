package com.example.amrietest.api

import android.content.Context
import com.example.amrietest.util.NetworkUtil
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit


private const val BASE_URL = "http://www.mocky.io/"

private fun buildClient(context: Context) =
    Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(OkHttpClient.Builder()
            .addInterceptor(ConnectivityInterceptor(context))
            .readTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(5, TimeUnit.SECONDS)
            .build())
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
        .create(IVideoService::class.java)

private class ConnectivityInterceptor(private val mContext: Context) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!NetworkUtil.isOnline(mContext)) {
            throw NoConnectivityException()
        }

        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }

}

class NoConnectivityException : IOException() {

    override val message: String?
        get() = "No connectivity exception"
}



object VideoService {
        @Volatile private var INSTANCE: IVideoService? = null

        fun getVideoService(context: Context): IVideoService  =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildClient(context).also { INSTANCE = it  }
            }
}
